// Note from me: I know using "clear: both" wogether with "float: left" is an old thing, but I have decided to leave it as it was when it was written.
// Today I will use display: inline-flex on the respective container class.

//Repository of all available passwords
//No interpuction characters allowed - if needed they can be added in <<letters>>.
const passwords = [
	"A co po czyjej wielkości, jak nie ma w głowie mądrości",
	"Apetyt rośnie w miarę jedzenia",
	"Baba z wozu koniom lżej",
	"Bez pracy nie ma kołaczy",
	"Elektryka prąd nie tyka",
	"Jak Kuba Bogu tak Bóg Kubie",
	"Jeśli wejdziesz międyz wrony będziesz musiałkrakać jak i one"
];

//Dynamic list of available characters
const letters = [
	"A", "Ą", "B", "C", "Ć",
	"D", "E", "Ę", "F", "G",
	"H", "I", "J", "K", "L",
	"Ł", "M", "N", "Ń", "O",
	"Ó", "P", "Q", "R", "S",
	"Ś", "T", "U", "V", "W",
	"X", "Y", "Z", "Ż", "Ź"
];

//Gets random password from available in <<passwords>>
const getRandomPassword = () => {
	const index = Math.floor(Math.random() * passwords.length);
	return passwords[index].toUpperCase();
};

//Draws all available characters on the screen
const showAlphabet = (currentGuess) => {
	let divContent = "";
	for(i = 0; i < letters.length; i++) {
		const element = "let" + i;
		divContent = divContent + '<div class="letter" onclick="check(' + i + ')" id="' + element + '">' + letters[i] + '</div>';

		//Breaks the line of characters based on defined maximum - in this case 7 in a row.
		if((1 + i) % 7 == 0) {
			divContent = divContent + '<div style="clear: both"></div>';
		}
	}
	
	document.getElementById("alphabet").innerHTML = divContent;
	showPassword(currentGuess);
};

//Draws current guess on the screen
const showPassword = (currentGuess) => {
	document.getElementById("board").innerHTML = currentGuess;
}

//Replaces all non-space characters with dashes
const prepareGuessString = (password) => {
	let output = "";
	const passwordLength = password.length;
	for(let i = 0; i < passwordLength; i++) {
		if(password.charAt(i)==" ") {
			output = output + " ";
		} else {
			output = output + "-";
		}
	}

	return output;
};

//Updates string replacing character at given location in given text with given symbol
const setChar = (text, location, symbol) => {
	if(location > text.length-1) {
		return text;
	} else {
		return text.substr(0, location) + symbol + text.substr(location + 1);
	}
}

//Game setup
const password = getRandomPassword();
let missed = 0;
let currentGuess = prepareGuessString(password);
const yes = new Audio("./audio/yes.wav");
const no = new Audio("./audio/no.wav");
window.onload = showAlphabet(currentGuess);

//Called every time any of available letters is clicked
const check = (num) => {
	let found = false;
	const passwordLength = password.length;

	//Checks if selected letter exist in the chosen password
	for(i = 0; i < passwordLength; i++) {
		if(password.charAt(i) == letters[num]) {
			currentGuess = setChar(currentGuess, i, letters[num]);
			found = true;
		}
	}

	//Finds clicked letter
	const elementId = "let" + num;
	const element = document.getElementById(elementId);

	//Removes reference to this function
	element.setAttribute("onclick", ";");

	//Guess was true, change letter layout to green
	if(found === true) {
		yes.play();
		element.style.background = "#003300";
		element.style.color = "#00c000";
		element.style.border = "3px solid #00c000";
		element.style.border = "default";
		showPassword(currentGuess);
	//Guess was false, change letter layout to red
	} else {
		no.play();
		element.style.background = "#330000";
		element.style.color = "#c00000";
		element.style.border = "3px solid #c00000";
		element.style.border = "default";
		
		missed++;
		const image = "img/s" + missed + ".jpg";
		document.getElementById("gallows").innerHTML = '<img src="'+image+'" alt="" />';
	}
	
	//If whole password has been guessed
	if(password === currentGuess) {
		document.getElementById("alphabet").innerHTML = "You Win! Password was: " + password + '<br/><br/><span class="reset" onclick="location.reload()">AGAIN?</span>';
	}
	
	//If all chances are used
	if(missed >= 9) {
		document.getElementById("alphabet").innerHTML = "You Lose! Password was: " + password + '<br/><br/><span class="reset" onclick="location.reload()">AGAIN?</span>';
	}
}