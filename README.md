# README #

Source: https://www.youtube.com/watch?v=9FVtiJHFCSU by Mirosław Zelent.
This is a hangman game based on the youtube tutorial, written ~2017, revisited and updated 2/10/2021.

### What is this repository for? ###

* Allows user to play a hangman game.
* Version 2.0

### How do I get set up? ###

* Download the repository or clone it on your local machine,
* Open index.html file in your browser,
* Enjoy!